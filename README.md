[TOC]



# Android Studio 2.2版本之前的NDK项目编译方式（两种）

- Android Studio 默认编译方式
- ndk-build

## 1.Android Studio 默认编译方式

### 1.1  创建带有 native 方法的类，build 项目

```java
//声明一个本地方法，用native关键字修饰
public native String stringFromat();
```



### 1.2 **生成与类名相关的 .h 文件** 

进入 app -> build -> intermediates -> classes -> debug 目录下执行（先配置好 JDK 的环境变量）：

```bash
javah com.jonyker.so.MainActivity
```

生成 **com_jonyker_so_MainActivity.h**文件 



### 1.3 创建cpp文件

在 main 文件夹下，新建 jni 目录，剪切 .h 文件到 jni 目录下，创建 hello.cpp 文件 。

```c

#include <com_jonyker_so_MainActivity.h>

/*
 * Class:     com_jonyker_so_MainActivity
 * Method:    stringFromat
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_jonyker_so_MainActivity_stringFromat
  (JNIEnv *env, jobject obj)
  {
    jstring str = (*env)->NewStringUTF(env, "HelloWorld from JNI !");
    return str;
  }

```



### 1.4 配置 build.gradle 文件

修改 app/build.gradle 文件， muduleName 为引入的 .so name , 直接运行项目。

```groovy
ndk{
     //生成的so文件名字，java调用C程序的代码中会用到该名字
     moduleName "hello"    
     //输出指定平台下的so库
     abiFilters "armeabi", "armeabi-v7a", "x86", 'x86_64', 'mips', 'mips64'
  }
```



## 2.ndk-build命令编译方式

新建一个项目，在 app 目录下（任目录下都可以）新建 jni 文件，添加 `Android.mk` 和 `Application.mk` 文件，以及 .h 文件（运用上一小节的方法生成）。 

**Android.mk文件**

```makefile
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := hello
LOCAL_SRC_FILES := D:\android\studio\work_g\WrapSO\app\src\main\jni\com_jonyker_so_MainActivity.c
include $(BUILD_SHARED_LIBRARY)
```

**Application.mk**

```makefile
代表生成所有平台的库文件。
APP_ABI := all

代表生成这三个平台的so文件。
APP_ABI := armeabi armeabi-v7a x86
```
**切换到 jni 目录下（配置好NDK环境变量）直接执行 ndk-build ， 生成 .so 文件。** 