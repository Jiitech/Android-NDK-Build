//
// Created by Administrator on 2017/6/22 0022.
//


#include <com_jonyker_so_MainActivity.h>

/*
 * Class:     com_jonyker_so_MainActivity
 * Method:    stringFromat
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_jonyker_so_MainActivity_stringFromat
  (JNIEnv *env, jobject obj)
  {
    jstring str = (*env)->NewStringUTF(env, "HelloWorld from JNI !");
    return str;
  }
