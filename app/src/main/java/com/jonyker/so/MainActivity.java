package com.jonyker.so;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private TextView mText;

    //使用静态代码块，表示我们要加载的资源文件为libhello.so
    static {
        /**
         * 这个位置的名字 需要和gradle文件中 moduleName "hello" 保持一致
         */
        System.loadLibrary("hello");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mText = (TextView) this.findViewById(R.id.text);
        mText.setText(stringFromat());

    }
    //声明一个本地方法，用native关键字修饰
    public native String stringFromat();

}
